import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AcessoRoutingModule } from './acesso-routing.module';
import { LoginComponent } from './login/login.component';
import { RegistroComponent } from './registro/registro.component';
import { RecuperarSenhaComponent } from './recuperar-senha/recuperar-senha.component';


@NgModule({
  declarations: [LoginComponent, RegistroComponent, RecuperarSenhaComponent],
  imports: [
    CommonModule,
    AcessoRoutingModule
  ]
})
export class AcessoModule { }
