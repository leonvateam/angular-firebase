import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InternoRoutingModule } from './interno-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';


@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    InternoRoutingModule
  ]
})
export class InternoModule { }
